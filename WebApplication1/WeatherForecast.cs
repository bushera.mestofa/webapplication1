namespace WebApplication1
{
    public class WeatherForecast
    {
        public string Version { get; set; } = "version 0.1";

        public DateTime Date { get; set; }

        public int TemperatureC { get; set; }

        public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);

        public string? Summary { get; set; }

        public string? AuthorName { get; set; }
    }
}
